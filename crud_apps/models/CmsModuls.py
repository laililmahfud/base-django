from django import forms
from django.db import models


class CmsModuls(models.Model):
    name = models.CharField(max_length=100)
    icon = models.CharField(max_length=100)
    controller = models.CharField(max_length=100)
    path = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        app_label = "crud_apps"
        db_table = "cms_moduls"


class CmsModulsForm(forms.ModelForm):
    class Meta:
        model = CmsModuls
        fields = "__all__"
