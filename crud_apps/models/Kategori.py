from django import forms
from django.db import models


class Kategori(models.Model):
   nama_kategori = models.CharField(max_length=100)
   class Meta:
       app_label  ='crud_apps'
       db_table  ='kategori'
class KategoriForm(forms.ModelForm):
   nama_kategori = forms.CharField(label='Nama Kategori',max_length=100,widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama Kategori'}))
   class Meta:
       fields  ='__all__'
       model  =Kategori
