from django import forms
from django.db import models
from .Role import *


class Users(models.Model):
    nama_users = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    telp = models.CharField(max_length=100)
    foto = models.FileField(upload_to='image/')
    password = models.CharField(max_length=100)
    id_role = models.ForeignKey(Role,on_delete=models.CASCADE)

    class Meta:
        app_label = 'crud_apps'
        db_table = 'users'


class UsersForm(forms.ModelForm):
    nama_users = forms.CharField(label='Nama Users', max_length=100,
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama Users'}))
    email = forms.CharField(label='Email', max_length=100,
                            widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))
    telp = forms.CharField(label='Telp', max_length=100,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Telp'}))
    foto = forms.FileField(label='Foto', max_length=100,
                           widget=forms.FileInput(attrs={'class': 'form-control', 'placeholder': 'Foto'}))
    password = forms.CharField(label='Password', max_length=100,
                               required=False,
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))

    id_role = forms.ModelChoiceField(
                queryset=Role.objects.all(),
                empty_label="Silahkan pilih role",
                label='Nama Role',
                widget=forms.Select(attrs={'class': 'form-control select2'})
            )


    class Meta:
        fields = '__all__'
        model = Users
