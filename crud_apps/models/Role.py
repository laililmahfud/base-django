from django import forms
from django.db import models


class Role(models.Model):
    nama_role = models.CharField(max_length=100)
    class Meta:
        app_label = 'crud_apps'
        db_table = 'role'

    def __str__(self):
        return u'{0}'.format(self.nama_role)

class RoleForm(forms.ModelForm):
    nama_role = forms.CharField(label='Nama Role', max_length=100,
                                widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama Role'}))

    class Meta:
        fields = '__all__'
        model = Role
