from django import forms
from django.db import models


class CmsMenus(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    icon = models.CharField(max_length=100)
    parent_id = models.IntegerField(null=True)
    sorting = models.IntegerField(default=0)
    path = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        app_label = "crud_apps"
        db_table = "cms_menus"


class CmsMenusForm(forms.ModelForm):
    class Meta:
        model = CmsMenus
        fields = "__all__"
