import os

from django.shortcuts import render

from crud_apps.templatetags import GOCrud
from crud_apps.templatetags.GOCrud import columnsTable

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

controller_dir = os.path.join(BASE_DIR, 'controller')


def index(request):
    # You can make your custom view in this

    columns = columnsTable('users')
    data = {
        "page_titel": "Dashboard",
        "icon_page": "fa fa-home",
        'data': columns
    }
    # GOCrud.makeRouter('tes', 'RoleController')

    # filepath = os.path.join(BASE_DIR, 'controller/tes.py')
    #
    # f = open(filepath, "a")
    # f.write("Hello Word !")
    return render(request, "views/index.html", data)
