from django.http import JsonResponse, HttpResponse
from rest_framework import serializers, viewsets

from crud_apps.models import Users, Role


# 'created_at': row.created_at.strftime("%Y-%m-%d")

def getList(request):
    query = Users.objects.all()
    data = []
    for row in query:
        try:
            role = Role.objects.get(id=row.id_role_id)
            nama_role = role.nama_role
        except Role.DoesNotExist:
            nama_role = ''
        key = {
            'nama_users': row.nama_users,
            "email": row.email,
            "foto": request.build_absolute_uri(row.foto.url),
            "role": nama_role
        }
        data.append(key)
    res = {
        "api_status": 1,
        "api_message": 'success',
        "result": data
    }
    return JsonResponse(res)


class SongSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = '__all__'


class SongViewSet(viewsets.ModelViewSet):
    queryset = Users.objects.all()
    serializer_class = SongSerializer
