from django.contrib import messages
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render
from crud_apps.models import RoleForm, Role

base_path = 'role'
icon_page = 'fa fa-list'


def getindex(request):
    btn_action = {
        'edit': True,
        "delete": True,
        "detail": True,
        "selected": True,
        "btn_add": True,
        "btn_export": False,
        "btn_import": False,
        # this button will be show on table
    }
    if request.GET.get('limit'):
        limit = request.GET.get('limit')
    else:
        limit = 10

    q = request.GET.get('q')
    if q:
        data_list = Role.objects.filter(id__icontains=q)
        # you can change and add your query filter in this line
    else:
        data_list = Role.objects.all()
        # this query to show all data
    page = request.GET.get('page', 1)
    paginator = Paginator(data_list, limit)

    try:
        list = paginator.page(page)
    except PageNotAnInteger:
        list = paginator.page(1)
    except EmptyPage:
        list = paginator.page(paginator.num_pages)

    result = {
        "page_titel": "Role",
        "icon_page": icon_page,
        "action": btn_action,
        "base_path": base_path,
        "data": list,
    }
    return render(request, 'views/role/index.html', result)


def getAdd(request):
    if request.method == 'POST':
        form = RoleForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'The data has been added !')
            if request.POST.get('submit') == 'Simpan & Tambah Lagi':
                return HttpResponseRedirect(request.POST.get('my_url'))
            else:
                return HttpResponseRedirect(request.POST.get('return_url'))
    else:
        form = RoleForm()
    result = {
        'page_titel': 'Tambah Role Baru',
        "icon_page": icon_page,
        "base_path": base_path,
        "form": form
    }
    return render(request, 'views/base/form.html', result)


def getEdit(request, id):
    if request.method == 'POST':
        data = Role.objects.get(id=id)
        form = RoleForm(request.POST, instance=data)
        if form.is_valid():
            form.save()
            messages.success(request, 'The data has been updated !')
            return HttpResponseRedirect(request.POST.get('return_url'))
    else:
        data = Role.objects.get(id=id)
        form = RoleForm(instance=data)
    result = {
        "page_titel": "Edit Role",
        "icon_page": icon_page,
        "base_path": base_path,
        "form": form,
        "edit": True
    }
    return render(request, 'views/base/form.html', result)


def getDelete(request, id):
    data = Role.objects.get(id=id)
    if data.delete():
        messages.success(request, 'The data has been deleted !')
    else:
        messages.warning(request, 'Error somehing when wrong !')
    return HttpResponseRedirect(request.GET.get('return_url'))


def getActionSelected(request):
    id_selected = request.POST.getlist('id_selected[]')
    if id_selected:
        act = Role.objects.filter(id__in=id_selected)
        if act.delete():
            messages.success(request, 'The data has been deleted !')
        else:
            messages.warning(request, 'Error somehing when wrong !')
    else:
        messages.warning(request, 'Please select one data !')
    return HttpResponseRedirect(request.POST.get('return_url'))


def getDetail(request,id):
    data = Role.objects.get(id=id)
    form = RoleForm(instance=data)
    result = {
       "page_titel": "Edit Role",
       "icon_page": icon_page,
       "base_path": base_path,
       "form": form,
       "detail": True
    }
    return render(request, 'views/base/form.html', result)