# from django.contrib import messages
# from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
# from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
# from django.shortcuts import render
#
# from crud_apps.models import RoleForm, Role
#
# base_path = "role"
# icon_page = "fa fa-tags"
#
#
# def index(request):
#     btn_action = {
#         "edit": True,
#         "delete": True,
#         "detail": True,
#         "selected": True,
#         "btn_add": True,
#         "btn_export": False,
#         "btn_import": False
#     }
#     if request.GET.get('limit'):
#         limit = request.GET.get('limit')
#     else:
#         limit = 10
#     q = request.GET.get('q')
#     if q:
#         role_list = Role.objects.filter(nama_role__icontains=q)
#     else:
#         role_list = Role.objects.all()
#
#     page = request.GET.get('page', 1)
#     paginator = Paginator(role_list, limit)
#
#     try:
#         role = paginator.page(page)
#     except PageNotAnInteger:
#         role = paginator.page(1)
#     except EmptyPage:
#         role = paginator.page(paginator.num_pages)
#
#     data = {
#         "page_titel": "Role",
#         "icon_page": icon_page,
#         "action": btn_action,
#         "base_path": base_path,
#         "data": role
#     }
#     return render(request, "views/role/index.html", data)
#
#
# def add(request):
#     if request.method == "POST":
#         form = RoleForm(request.POST)
#         if form.is_valid():
#             form.save()
#             messages.success(request, 'The data has been added !')
#             if request.POST.get('submit') == 'Simpan & Tambah Lagi':
#                 return HttpResponseRedirect(request.POST.get('my_url'))
#             else:
#                 return HttpResponseRedirect(request.POST.get('return_url'))
#     else:
#         form = RoleForm()
#     data = {
#         "page_titel": "Tambah Role Baru",
#         "icon_page": icon_page,
#         "base_path": base_path,
#         "form": form
#     }
#     return render(request, "views/base/form.html", data)
#
#
# def edit(request, id):
#     if request.method == "POST":
#         role = Role.objects.get(id=id)
#         form = RoleForm(request.POST, instance=role)
#         if form.is_valid():
#             form.save()
#             messages.success(request, 'The data has been updated !')
#             return HttpResponseRedirect(request.POST.get('return_url'))
#     else:
#         role = Role.objects.get(id=id)
#         form = RoleForm(instance=role)
#     data = {
#         "page_titel": "Edit Role",
#         "icon_page": icon_page,
#         "base_path": base_path,
#         "form": form,
#         "edit": True
#     }
#     return render(request, "views/base/form.html", data)
#
#
# def delete(request, id):
#     role = Role.objects.get(id=id)
#     if role.delete():
#         messages.success(request, 'The data has been deleted !')
#     else:
#         messages.warning(request, 'Error somehing when wrong !')
#     return HttpResponseRedirect(request.GET.get('return_url'))
#
#
# def selected(request):
#     id_selected = request.POST.getlist('id_selected[]')
#
#     if id_selected:
#         role = Role.objects.filter(id__in=id_selected)
#         if role.delete():
#             messages.success(request, 'The data has been deleted !')
#         else:
#             messages.warning(request, 'Error somehing when wrong !')
#     else:
#         messages.warning(request, 'Please select one data !')
#     return HttpResponseRedirect(request.POST.get('return_url'))
