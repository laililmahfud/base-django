import os
import string

from datetime import datetime
from django.db import connection
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from crud.settings import BASE_DIR
from crud_apps.models import  CmsModuls, CmsMenus
from crud_apps.templatetags import GOCrud
from crud_apps.templatetags.GOCrud import columnsTable


def step1(request):
    if request.method == "POST":
        request.session['table'] = request.POST.get('table')
        request.session['name'] = request.POST.get('name')
        request.session['path'] = request.POST.get('path')
        request.session['icon'] = request.POST.get('icon')
        request.session['create_menu'] = request.POST.get('create_menu')
        request.session['model_name'] = string.capwords(request.POST.get('table')).replace(" ", '')
        request.session['controller_name'] = "Admin" + request.POST.get('name') + "Controller"
        # controller_name = string.capwords("Admin" + name + "Controller").replace(" ", '')

        return HttpResponseRedirect("step2")

    tables = connection.introspection.table_names()
    # model_tables = connection.introspection.installed_models(tables)
    table = [x for x in tables if not x.startswith('cms_')]
    sql = "select * from cms_menus"
    rows = GOCrud.query(sql)
    data = {
        "page_titel": "Module Generator",
        "icon_page": "fa fa-database",
        "list_icon": GOCrud.ListIcon,
        "table": table,
        "data": rows,
        "tes": GOCrud.columnsTable('users')
    }

    return render(request, "GOCrud/module_generator/step1.html", data)


def step2(request):
    table = request.session.get('table')
    name = request.session.get('name')
    path = request.session.get('path')
    icon = request.session.get('icon')
    model_name = request.session.get('model_name')
    controller_name = request.session.get('controller_name')
    table = str(table).strip('[]')
    if request.method == "POST":
        columns = request.POST.getlist('column[]')
        label = request.POST.getlist('label[]')
        save = CmsModuls()
        save.icon = icon
        save.controller = controller_name
        save.path = path
        save.save()

        if request.session['create_menu']:
            menu = CmsMenus()
            menu.name = name
            menu.type = 'url'
            menu.icon = icon
            menu.sorting = CmsMenus.objects.count() + 1
            menu.path = path
            menu.save()

        GOCrud.makeModelandForm(model_name, model_name + "Form", table, columns, label)
        GOCrud.makeController(controller_name, model_name + "Form", model_name, path, icon,name)
        GOCrud.makeIndexView(path)
        GOCrud.makeRouter(request.session.get('path'), controller_name)
        return HttpResponseRedirect('/'+path)

    data = {
        "page_titel": "Module Generator",
        "icon_page": "fa fa-database",
        "columns": GOCrud.columnsTable(table)
    }

    return render(request, "GOCrud/module_generator/step2.html", data)


def sampleAPi():
    query = CmsMenus.objects.all()
    res = {
        "api_status": 1,
        "api_message": 'success',
        "result": list(query.values())
    }
    return JsonResponse(res)
