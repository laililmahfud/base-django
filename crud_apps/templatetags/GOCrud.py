import os
import string
from random import randint

from django import template
from django.db import connection
from django.http import JsonResponse

from crud_apps.models import CmsMenus, CmsModuls

register = template.Library()
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))



@register.simple_tag
def app_name():
    return "crud_apps"


@register.simple_tag
def random_number():
    return randint(0, 999)


@register.simple_tag
def listMenu():
    menu = CmsMenus.objects.all()
    return menu


@register.simple_tag
def listModule():
    module = CmsModuls.objects.all()
    return module


@register.simple_tag
def query(sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    rows = dictfetchall(cursor)
    return rows


@register.simple_tag
def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


@register.simple_tag
def ucWord(string):
    return (string).title().replace("_", ' ')


@register.simple_tag
def columnsTable(table):
    db = connection.settings_dict['NAME']
    sql = "SELECT COLUMN_NAME,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME = '%s' and COLUMN_KEY!='PRI'" % (
        db, table)
    columns = query(sql)
    return columns


@register.simple_tag
def get_dbname():
    db_name = connection.settings_dict['NAME']
    return db_name


@register.simple_tag
def url_replace(request, field, value):
    dict_ = request.GET.copy()

    dict_[field] = value

    return dict_.urlencode()

@register.simple_tag
def handle_uploaded_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

@register.simple_tag
def ListIcon():
    icon = {
        "glass",
        "music",
        "search",
        "envelope-o",
        "heart",
        "star",
        "star-o",
        "user",
        "film",
        "th-large",
        "th",
        "th-list",
        "check",
        "remove",
        "close",
        "times",
        "search-plus",
        "search-minus",
        "power-off",
        "signal",
        "gear",
        "cog",
        "trash-o",
        "home",
        "file-o",
        "clock-o",
        "road",
        "download",
        "arrow-circle-o-down",
        "arrow-circle-o-up",
        "inbox",
        "play-circle-o",
        "rotate-right",
        "repeat",
        "refresh",
        "list-alt",
        "lock",
        "flag",
        "headphones",
        "volume-off",
        "volume-down",
        "volume-up",
        "qrcode",
        "barcode",
        "tag",
        "tags",
        "book",
        "bookmark",
        "print",
        "camera",
        "font",
        "bold",
        "italic",
        "text-height",
        "text-width",
        "align-left",
        "align-center",
        "align-right",
        "align-justify",
        "list",
        "dedent",
        "outdent",
        "indent",
        "video-camera",
        "photo",
        "image",
        "picture-o",
        "pencil",
        "map-marker",
        "adjust",
        "tint",
        "edit",
        "pencil-square-o",
        "share-square-o",
        "check-square-o",
        "arrows",
        "step-backward",
        "fast-backward",
        "backward",
        "play",
        "pause",
        "stop",
        "forward",
        "fast-forward",
        "step-forward",
        "eject",
        "chevron-left",
        "chevron-right",
        "plus-circle",
        "minus-circle",
        "times-circle",
        "check-circle",
        "question-circle",
        "info-circle",
        "crosshairs",
        "times-circle-o",
        "check-circle-o",
        "ban",
        "arrow-left",
        "arrow-right",
        "arrow-up",
        "arrow-down",
        "mail-forward",
        "share",
        "expand",
        "compress",
        "plus",
        "minus",
        "asterisk",
        "exclamation-circle",
        "gift",
        "leaf",
        "fire",
        "eye",
        "eye-slash",
        "warning",
        "exclamation-triangle",
        "plane",
        "calendar",
        "random",
        "comment",
        "magnet",
        "chevron-up",
        "chevron-down",
        "retweet",
        "shopping-cart",
        "folder",
        "folder-open",
        "arrows-v",
        "arrows-h",
        "bar-chart-o",
        "bar-chart",
        "twitter-square",
        "facebook-square",
        "camera-retro",
        "key",
        "gears",
        "cogs",
        "comments",
        "thumbs-o-up",
        "thumbs-o-down",
        "star-half",
        "heart-o",
        "sign-out",
        "linkedin-square",
        "thumb-tack",
        "external-link",
        "sign-in",
        "trophy",
        "github-square",
        "upload",
        "lemon-o",
        "phone",
        "square-o",
        "bookmark-o",
        "phone-square",
        "twitter",
        "facebook-f",
        "facebook",
        "github",
        "unlock",
        "credit-card",
        "feed",
        "rss",
        "hdd-o",
        "bullhorn",
        "bell",
        "certificate",
        "hand-o-right",
        "hand-o-left",
        "hand-o-up",
        "hand-o-down",
        "arrow-circle-left",
        "arrow-circle-right",
        "arrow-circle-up",
        "arrow-circle-down",
        "globe",
        "wrench",
        "tasks",
        "filter",
        "briefcase",
        "arrows-alt",
        "group",
        "users",
        "chain",
        "link",
        "cloud",
        "flask",
        "cut",
        "scissors",
        "copy",
        "files-o",
        "paperclip",
        "save",
        "floppy-o",
        "square",
        "navicon",
        "reorder",
        "bars",
        "list-ul",
        "list-ol",
        "strikethrough",
        "underline",
        "table",
        "magic",
        "truck",
        "pinterest",
        "pinterest-square",
        "google-plus-square",
        "google-plus",
        "money",
        "caret-down",
        "caret-up",
        "caret-left",
        "caret-right",
        "columns",
        "unsorted",
        "sort",
        "sort-down",
        "sort-desc",
        "sort-up",
        "sort-asc",
        "envelope",
        "linkedin",
        "rotate-left",
        "undo",
        "legal",
        "gavel",
        "dashboard",
        "tachometer",
        "comment-o",
        "comments-o",
        "flash",
        "bolt",
        "sitemap",
        "umbrella",
        "paste",
        "clipboard",
        "lightbulb-o",
        "exchange",
        "cloud-download",
        "cloud-upload",
        "user-md",
        "stethoscope",
        "suitcase",
        "bell-o",
        "coffee",
        "cutlery",
        "file-text-o",
        "building-o",
        "hospital-o",
        "ambulance",
        "medkit",
        "fighter-jet",
        "beer",
        "h-square",
        "plus-square",
        "angle-double-left",
        "angle-double-right",
        "angle-double-up",
        "angle-double-down",
        "angle-left",
        "angle-right",
        "angle-up",
        "angle-down",
        "desktop",
        "laptop",
        "tablet",
        "mobile-phone",
        "mobile",
        "circle-o",
        "quote-left",
        "quote-right",
        "spinner",
        "circle",
        "mail-reply",
        "reply",
        "github-alt",
        "folder-o",
        "folder-open-o",
        "smile-o",
        "frown-o",
        "meh-o",
        "gamepad",
        "keyboard-o",
        "flag-o",
        "flag-checkered",
        "terminal",
        "code",
        "mail-reply-all",
        "reply-all",
        "star-half-empty",
        "star-half-full",
        "star-half-o",
        "location-arrow",
        "crop",
        "code-fork",
        "unlink",
        "chain-broken",
        "question",
        "info",
        "exclamation",
        "superscript",
        "subscript",
        "eraser",
        "puzzle-piece",
        "microphone",
        "microphone-slash",
        "shield",
        "calendar-o",
        "fire-extinguisher",
        "rocket",
        "maxcdn",
        "chevron-circle-left",
        "chevron-circle-right",
        "chevron-circle-up",
        "chevron-circle-down",
        "html5",
        "css3",
        "anchor",
        "unlock-alt",
        "bullseye",
        "ellipsis-h",
        "ellipsis-v",
        "rss-square",
        "play-circle",
        "ticket",
        "minus-square",
        "minus-square-o",
        "level-up",
        "level-down",
        "check-square",
        "pencil-square",
        "external-link-square",
        "share-square",
        "compass",
        "toggle-down",
        "caret-square-o-down",
        "toggle-up",
        "caret-square-o-up",
        "toggle-right",
        "caret-square-o-right",
        "euro",
        "eur",
        "gbp",
        "dollar",
        "usd",
        "rupee",
        "inr",
        "cny",
        "rmb",
        "yen",
        "jpy",
        "ruble",
        "rouble",
        "rub",
        "won",
        "krw",
        "bitcoin",
        "btc",
        "file",
        "file-text",
        "sort-alpha-asc",
        "sort-alpha-desc",
        "sort-amount-asc",
        "sort-amount-desc",
        "sort-numeric-asc",
        "sort-numeric-desc",
        "thumbs-up",
        "thumbs-down",
        "youtube-square",
        "youtube",
        "xing",
        "xing-square",
        "youtube-play",
        "dropbox",
        "stack-overflow",
        "instagram",
        "flickr",
        "adn",
        "bitbucket",
        "bitbucket-square",
        "tumblr",
        "tumblr-square",
        "long-arrow-down",
        "long-arrow-up",
        "long-arrow-left",
        "long-arrow-right",
        "apple",
        "windows",
        "android",
        "linux",
        "dribbble",
        "skype",
        "foursquare",
        "trello",
        "female",
        "male",
        "gittip",
        "gratipay",
        "sun-o",
        "moon-o",
        "archive",
        "bug",
        "vk",
        "weibo",
        "renren",
        "pagelines",
        "stack-exchange",
        "arrow-circle-o-right",
        "arrow-circle-o-left",
        "toggle-left",
        "caret-square-o-left",
        "dot-circle-o",
        "wheelchair",
        "vimeo-square",
        "turkish-lira",
        "try",
        "plus-square-o",
        "space-shuttle",
        "slack",
        "envelope-square",
        "wordpress",
        "openid",
        "institution",
        "bank",
        "university",
        "mortar-board",
        "graduation-cap",
        "yahoo",
        "google",
        "reddit",
        "reddit-square",
        "stumbleupon-circle",
        "stumbleupon",
        "delicious",
        "digg",
        "pied-piper",
        "pied-piper-alt",
        "drupal",
        "joomla",
        "language",
        "fax",
        "building",
        "child",
        "paw",
        "spoon",
        "cube",
        "cubes",
        "behance",
        "behance-square",
        "steam",
        "steam-square",
        "recycle",
        "automobile",
        "car",
        "cab",
        "taxi",
        "tree",
        "spotify",
        "deviantart",
        "soundcloud",
        "database",
        "file-pdf-o",
        "file-word-o",
        "file-excel-o",
        "file-powerpoint-o",
        "file-photo-o",
        "file-picture-o",
        "file-image-o",
        "file-zip-o",
        "file-archive-o",
        "file-sound-o",
        "file-audio-o",
        "file-movie-o",
        "file-video-o",
        "file-code-o",
        "vine",
        "codepen",
        "jsfiddle",
        "life-bouy",
        "life-buoy",
        "life-saver",
        "support",
        "life-ring",
        "circle-o-notch",
        "ra",
        "rebel",
        "ge",
        "empire",
        "git-square",
        "git",
        "y-combinator-square",
        "yc-square",
        "hacker-news",
        "tencent-weibo",
        "qq",
        "wechat",
        "weixin",
        "send",
        "paper-plane",
        "send-o",
        "paper-plane-o",
        "history",
        "circle-thin",
        "header",
        "paragraph",
        "sliders",
        "share-alt",
        "share-alt-square",
        "bomb",
        "soccer-ball-o",
        "futbol-o",
        "tty",
        "binoculars",
        "plug",
        "slideshare",
        "twitch",
        "yelp",
        "newspaper-o",
        "wifi",
        "calculator",
        "paypal",
        "google-wallet",
        "cc-visa",
        "cc-mastercard",
        "cc-discover",
        "cc-amex",
        "cc-paypal",
        "cc-stripe",
        "bell-slash",
        "bell-slash-o",
        "trash",
        "copyright",
        "at",
        "eyedropper",
        "paint-brush",
        "birthday-cake",
        "area-chart",
        "pie-chart",
        "line-chart",
        "lastfm",
        "lastfm-square",
        "toggle-off",
        "toggle-on",
        "bicycle",
        "bus",
        "ioxhost",
        "angellist",
        "cc",
        "shekel",
        "sheqel",
        "ils",
        "meanpath",
        "buysellads",
        "connectdevelop",
        "dashcube",
        "forumbee",
        "leanpub",
        "sellsy",
        "shirtsinbulk",
        "simplybuilt",
        "skyatlas",
        "cart-plus",
        "cart-arrow-down",
        "diamond",
        "ship",
        "user-secret",
        "motorcycle",
        "street-view",
        "heartbeat",
        "venus",
        "mars",
        "mercury",
        "intersex",
        "transgender",
        "transgender-alt",
        "venus-double",
        "mars-double",
        "venus-mars",
        "mars-stroke",
        "mars-stroke-v",
        "mars-stroke-h",
        "neuter",
        "genderless",
        "facebook-official",
        "pinterest-p",
        "whatsapp",
        "server",
        "user-plus",
        "user-times",
        "hotel",
        "bed",
        "viacoin",
        "train",
        "subway",
        "medium",
        "yc",
        "y-combinator",
        "optin-monster",
        "opencart",
        "expeditedssl",
        "battery-4",
        "battery-full",
        "battery-3",
        "battery-three-quarters",
        "battery-2",
        "battery-half",
        "battery-1",
        "battery-quarter",
        "battery-0",
        "battery-empty",
        "mouse-pointer",
        "i-cursor",
        "object-group",
        "object-ungroup",
        "sticky-note",
        "sticky-note-o",
        "cc-jcb",
        "cc-diners-club",
        "clone",
        "balance-scale",
        "hourglass-o",
        "hourglass-1",
        "hourglass-start",
        "hourglass-2",
        "hourglass-half",
        "hourglass-3",
        "hourglass-end",
        "hourglass",
        "hand-grab-o",
        "hand-rock-o",
        "hand-stop-o",
        "hand-paper-o",
        "hand-scissors-o",
        "hand-lizard-o",
        "hand-spock-o",
        "hand-pointer-o",
        "hand-peace-o",
        "trademark",
        "registered",
        "creative-commons",
        "gg",
        "gg-circle",
        "tripadvisor",
        "odnoklassniki",
        "odnoklassniki-square",
        "get-pocket",
        "wikipedia-w",
        "safari",
        "chrome",
        "firefox",
        "opera",
        "internet-explorer",
        "tv",
        "television",
        "contao",
        "500px",
        "amazon",
        "calendar-plus-o",
        "calendar-minus-o",
        "calendar-times-o",
        "calendar-check-o",
        "industry",
        "map-pin",
        "map-signs",
        "map-o",
        "map",
        "commenting",
        "commenting-o",
        "houzz",
        "vimeo",
        "black-tie",
        "fonticons",
        "reddit-alien",
        "edge",
        "credit-card-alt",
        "codiepie",
        "modx",
        "fort-awesome",
        "usb",
        "product-hunt",
        "mixcloud",
        "scribd",
        "pause-circle",
        "pause-circle-o",
        "stop-circle",
        "stop-circle-o",
        "shopping-bag",
        "shopping-basket",
        "hashtag",
        "bluetooth",
        "bluetooth-b",
        "percent",
    }
    return icon


@register.simple_tag
def makeRouter(path, controller):
    filepath = os.path.join(BASE_DIR, 'routes.py')

    f = open(filepath, "a")
    f.write("\n\n\nfrom crud_apps.controller import " + controller + "\n")
    f.write("url" + path + " = [\n")
    f.write("       path(admin_path + '/" + path + "', " + controller + ".getindex, name='" + path + "'),\n")
    f.write("       path(admin_path + '/" + path + "/add', " + controller + ".getAdd, name='" + path + "_add'),\n")
    f.write(
        "       path(admin_path + '/" + path + "/edit/<int:id>', " + controller + ".getEdit, name='" + path + "_edit'),\n")
    f.write(
        "       path(admin_path + '/" + path + "/delete/<int:id>', " + controller + ".getDelete, name='" + path + "_delete'),\n")
    f.write(
        "       path(admin_path + '/" + path + "/action-selected', " + controller + ".getActionSelected, name='" + path + "_selected'),\n")
    f.write("]\n")
    f.write("urlpatterns += url" + path + "")


@register.simple_tag
def makeModelandForm(model, form, table, columns, label):
    filepath = os.path.join(BASE_DIR, 'models/' + model + '.py')
    f = open(filepath, "a")
    f.write("from django import forms\n")
    f.write("from django.db import models\n\n\n")
    f.write("class " + model + "(models.Model):\n")
    for col in columns:
        f.write("   " + col + " = models.CharField(max_length=100)\n")
    f.write("   class Meta:\n")
    f.write("       app_label  ='crud_apps'\n")
    f.write("       db_table  ='" + table + "'\n")

    f.write("class " + form + "(forms.ModelForm):\n")
    for i in range(len(columns)):
        f.write("   " + columns[i] + " = forms.CharField(label='" + label[
            i] + "',max_length=100,widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': '" + label[
                    i] + "'}))\n")
    f.write("   class Meta:\n")
    f.write("       fields  ='__all__'\n")
    f.write("       model  =" + model + "\n")

    path = os.path.join(BASE_DIR, 'models/__init__.py')
    init = open(path, "a")
    init.write("\nfrom ." + model + " import *\n")


@register.simple_tag
def makeController(name, form, model, path, icon, title):
    filepath = os.path.join(BASE_DIR, 'controller/' + name + '.py')
    f = open(filepath, "a")
    f.write("from django.contrib import messages\n")
    f.write("from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage\n")
    f.write("from django.http import HttpResponseRedirect, JsonResponse, HttpResponse\n")
    f.write("from django.shortcuts import render\n")
    f.write("from crud_apps.models import " + form + ", " + model + "\n")
    f.write("base_path = '" + path + "'\n")
    f.write("icon_page = '" + icon + "'\n")
    f.write("def getindex(request):\n")
    f.write("   btn_action = {\n")
    f.write("       'edit': True,\n")
    f.write('        "delete": True,\n')
    f.write('        "detail": True,\n')
    f.write('        "selected": True,\n')
    f.write('        "btn_add": True,\n')
    f.write('        "btn_export": False,\n')
    f.write('        "btn_import": False,\n')
    f.write("        #this button will be show on table\n")
    f.write('   }\n')
    f.write("   if request.GET.get('limit'):\n")
    f.write("        limit = request.GET.get('limit')\n")
    f.write("   else:\n")
    f.write("        limit = 10\n\n")
    f.write("   q = request.GET.get('q')\n")
    f.write("   if q:\n")
    f.write("        data_list = " + model + ".objects.filter(id__icontains=q)\n")
    f.write("        #you can change and add your query filter in this line\n")
    f.write("   else:\n")
    f.write("        data_list = " + model + ".objects.all()\n")
    f.write("        #this query to show all data\n")
    f.write("   page = request.GET.get('page', 1)\n")
    f.write("   paginator = Paginator(data_list, limit)\n\n")
    f.write("   try:\n")
    f.write("       list = paginator.page(page)\n")
    f.write("   except PageNotAnInteger:\n")
    f.write("       list = paginator.page(1)\n")
    f.write("   except EmptyPage:\n")
    f.write("       list = paginator.page(paginator.num_pages)\n\n")
    f.write("   result = {\n")
    f.write('       "page_titel": "' + title + '",\n')
    f.write('       "icon_page": icon_page,\n')
    f.write('       "action": btn_action,\n')
    f.write('       "base_path": base_path,\n')
    f.write('       "data": list,\n')
    f.write("   }\n")
    f.write("   return render(request, 'views/" + path + "/index.html', result)\n\n\n")

    f.write("def getAdd(request):\n")
    f.write("   if request.method == 'POST':\n")
    f.write("       form = " + form + "(request.POST)\n")
    f.write("       if form.is_valid():\n")
    f.write("           form.save()\n")
    f.write("           messages.success(request, 'The data has been added !')\n")
    f.write("           if request.POST.get('submit') == 'Simpan & Tambah Lagi':\n")
    f.write("               return HttpResponseRedirect(request.POST.get('my_url'))\n")
    f.write("           else:\n")
    f.write("               return HttpResponseRedirect(request.POST.get('return_url'))\n")
    f.write("   else:\n")
    f.write("       form = " + form + "()\n")
    f.write("   result = {\n")
    f.write("       'page_titel': 'Tambah " + title + " Baru',\n")
    f.write('       "icon_page": icon_page,\n')
    f.write('       "base_path": base_path,\n')
    f.write('       "form": form\n')
    f.write("   }\n")
    f.write("   return render(request, 'views/base/form.html', result)\n\n\n")

    f.write("def getEdit(request,id):\n")
    f.write("   if request.method == 'POST':\n")
    f.write("       data = " + model + ".objects.get(id=id)\n")
    f.write("       form = " + form + "(request.POST, instance=data)\n")
    f.write("       if form.is_valid():\n")
    f.write("           form.save()\n")
    f.write("           messages.success(request, 'The data has been updated !')\n")
    f.write("           return HttpResponseRedirect(request.POST.get('return_url'))\n")
    f.write("   else:\n")
    f.write("       data = " + model + ".objects.get(id=id)\n")
    f.write("       form = " + form + "(instance=data)\n")
    f.write("   result = {\n")
    f.write('       "page_titel": "Edit ' + title + '",\n')
    f.write('       "icon_page": icon_page,\n')
    f.write('       "base_path": base_path,\n')
    f.write('       "form": form,\n')
    f.write('       "edit": True\n')
    f.write("   }\n")
    f.write("   return render(request, 'views/base/form.html', result)\n\n\n")

    f.write("def getDelete(request, id):\n")
    f.write("   data = " + model + ".objects.get(id=id)\n")
    f.write("   if data.delete():\n")
    f.write("       messages.success(request, 'The data has been deleted !')\n")
    f.write("   else:\n")
    f.write("       messages.warning(request, 'Error somehing when wrong !')\n")
    f.write("   return HttpResponseRedirect(request.GET.get('return_url'))\n\n\n")

    f.write("def getActionSelected(request):\n")
    f.write("   id_selected = request.POST.getlist('id_selected[]')\n")
    f.write("   if id_selected:\n")
    f.write("       act = " + model + ".objects.filter(id__in=id_selected)\n")
    f.write("       if act.delete():\n")
    f.write("           messages.success(request, 'The data has been deleted !')\n")
    f.write("       else:\n")
    f.write("           messages.warning(request, 'Error somehing when wrong !')\n")
    f.write("   else:\n")
    f.write("       messages.warning(request, 'Please select one data !')\n")
    f.write("   return HttpResponseRedirect(request.POST.get('return_url'))\n")


@register.simple_tag
def makeIndexView(path):
    os.mkdir(os.path.join(BASE_DIR, 'resources/views', path))
    filepath = os.path.join(BASE_DIR, 'resources/views/' + path + '/index.html')
    f = open(filepath, "a")
    f.write("{% extends 'theme/base.html' %}\n")
    f.write("{% block content %}\n")
    f.write('<div class="row">\n')
    f.write('   <div class="col-xs-12">\n')
    f.write('       <div class="box">\n')
    f.write('           <div class="box-header">\n')
    f.write('               {% include "theme/table_header.html" %}\n')
    f.write('           </div>\n')
    f.write('           <div class="box-body table-responsive no-padding">\n')
    f.write('               <form action="{{ request.path }}/action-selected" method="POST" id="form-table">\n')
    f.write('                   {% csrf_token %}\n')
    f.write('                   <input type="hidden" name="return_url" value="{{ request.get_full_path }}">\n')
    f.write('                   <table class="table table-hover table-bordered table-default">\n')
    f.write('                       <tr>\n')
    f.write('                           {% if action.selected == True %}\n')
    f.write('                               <th style="width: 30px" id="">\n')
    f.write('                                   <input type="checkbox" name="" id="checkall">\n')
    f.write('                               </th>\n')
    f.write('                           {% endif %}\n')
    f.write('                           <th>Your Columns header</th>\n')
    f.write('                           <th class="text-right">Aksi</th>\n')
    f.write('                       </tr>\n')
    f.write('                       {% if data == None %}\n')
    f.write(
        '                           <tr><td colspan="4" class="text-center">This No Data Found In Table !</td></tr>\n')
    f.write('                       {% else %}\n')
    f.write('                           {% for row in data %}\n')
    f.write('                               <tr>\n')
    f.write('                                   {% if action.selected == True %}\n')
    f.write('                                       <td>\n')
    f.write(
        '                                           <input class="checkbox" type="checkbox" name="id_selected[]" value="{{ row.id }}" id="">\n')
    f.write('                                       </td>\n')
    f.write('                                   {% endif %}\n')
    f.write('                                   <td>Your data will be call here</td>\n')
    f.write('                                   <td class="text-right">\n')
    f.write('                                       {% if action.detail == True %}\n')
    f.write(
        '                                           <a href="{{ base_path }}/detail/{{ row.id }}?return_url={{ request.get_full_path }}" class="btn btn-xs btn-detail btn-primary"><i class="fa fa-eye"></i></a>\n')
    f.write('                                       {% endif %}\n')
    f.write('                                       {% if action.edit == True %}\n')
    f.write(
        '                                           <a href="{{ base_path }}/edit/{{ row.id }}?return_url={{ request.get_full_path }}" class="btn btn-xs btn-edit btn-success"><i class="fa fa-pencil"></i></a>\n')
    f.write('                                       {% endif %}\n')
    f.write('                                       {% if action.delete == True %}\n')
    f.write(
        '                                           <a href="javascript:;" data-path="{{ base_path }}"  data-id="{{ row.id }}" data-url="{{ request.get_full_path }}" class="btn btn-xs btn-hapus btn-warning btn-delete-table"><i class="fa fa-trash"></i></a>\n')
    f.write('                                       {% endif %}\n')
    f.write('                                   </td>\n')
    f.write('                               </tr>\n')
    f.write('                           {% endfor %}\n')
    f.write('                       {% endif %}\n')
    f.write('                   </table>\n')
    f.write('                   <div class="col-sm-12">\n')
    f.write('                       {% include "theme/pagination.html" %}\n')
    f.write('                   </div>\n')
    f.write('               </form>\n')
    f.write('           </div>\n')
    f.write('       </div>\n')
    f.write('   </div>\n')
    f.write('</div>\n')
    f.write('{% endblock %}\n')
