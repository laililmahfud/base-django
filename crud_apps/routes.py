from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework import routers

from crud_apps.GOCrud.controller import ModuleGenerator
from crud_apps.controller import IndexController, RoleController, ApiUsersController

admin_path = "admin"
api_path = "api"
urlpatterns = [
    path(admin_path + "/", IndexController.index, name="home"),
    path(admin_path + "/module-generator/step1", ModuleGenerator.step1, name="add-module-1"),
    path(admin_path + "/module-generator/step2", ModuleGenerator.step2, name="add-module-2"),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from crud_apps.controller import AdminUsersController

urlusers = [
    path(admin_path + '/users', AdminUsersController.getindex, name='users'),
    path(admin_path + '/users/add', AdminUsersController.getAdd, name='users_add'),
    path(admin_path + '/users/edit/<int:id>', AdminUsersController.getEdit, name='users_edit'),
    path(admin_path + '/users/delete/<int:id>', AdminUsersController.getDelete, name='users_delete'),
    path(admin_path + '/users/detail/<int:id>', AdminUsersController.getDetail, name='users_detail'),
    path(admin_path + '/users/action-selected', AdminUsersController.getActionSelected, name='users_selected'),
]
urlpatterns += urlusers

from crud_apps.controller import AdminRoleController

urlrole = [
    path(admin_path + '/role', AdminRoleController.getindex, name='role'),
    path(admin_path + '/role/add', AdminRoleController.getAdd, name='role_add'),
    path(admin_path + '/role/edit/<int:id>', AdminRoleController.getEdit, name='role_edit'),
    path(admin_path + '/role/delete/<int:id>', AdminRoleController.getDelete, name='role_delete'),
    path(admin_path + '/role/detail/<int:id>', AdminRoleController.getDetail, name='role_detail'),
    path(admin_path + '/role/action-selected', AdminRoleController.getActionSelected, name='role_selected'),
]
urlpatterns += urlrole

urlapi = [
    path(api_path + '/users', ApiUsersController.getList, name='api-users'),
]
urlpatterns += urlapi

router = routers.DefaultRouter()
router.register(r'song', ApiUsersController.SongViewSet)
urlapi2 = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns += urlapi2



from crud_apps.controller import AdminKategoriController
urlkategori = [
       path(admin_path + '/kategori', AdminKategoriController.getindex, name='kategori'),
       path(admin_path + '/kategori/add', AdminKategoriController.getAdd, name='kategori_add'),
       path(admin_path + '/kategori/edit/<int:id>', AdminKategoriController.getEdit, name='kategori_edit'),
       path(admin_path + '/kategori/delete/<int:id>', AdminKategoriController.getDelete, name='kategori_delete'),
       path(admin_path + '/kategori/action-selected', AdminKategoriController.getActionSelected, name='kategori_selected'),
]
urlpatterns += urlkategori

