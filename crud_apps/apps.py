from django.apps import AppConfig


class CrudAppsConfig(AppConfig):
    name = 'crud_apps'


def get_app_config(param):
    return None