$(function () {
    $('.select2').select2()
    $(".table-default #checkall").click(function () {
        var is_checked = $(this).is(":checked");
        $(".table-default .checkbox").prop("checked", !is_checked).trigger("click");
    })
})

function deleteRecord(url) {
    swal({
        title: "Apakah anda yakin ?",
        text: "Anda tidak akan dapat mengembalikan data anda!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        location.href=url
      }
    });
}
function deleteSelected() {
    swal({
        title: "Konfirmasi",
        text: "Apakah anda yakin untuk menghapus data terpilih ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#form-table').submit();
      }
    });
}
$('.btn-delete-table').on('click',function () {
    id = $(this).attr('data-id')
    return_url = $(this).attr('data-url')
    path = $(this).attr('data-path')
    url = path+'/delete/'+id+'?return_url='+return_url
    deleteRecord(url)
})